import logging

def FunctionDecoratorMetaClassFactory(dec_applicator, decorator):
    class MetaClass(type):
        def __new__(meta, classname, bases, classDict):
            newClassDict = dec_applicator(decorator, classDict)
            return type.__new__(meta, classname, bases, newClassDict)
    return MetaClass

#example decorator_applicator
def decorator_applicator(decorator):
    newClassDict = {}
    for attributeName, attribute in classDict.items():
        if type(attribute) == FunctionType:
            attribute = decorator(attribute)
        newClassDict[attributeName] = attribute
    return newClassDict

class Singleton(type):
    def __init__(cls, name, bases, dict):
        super(Singleton, cls).__init__(name, bases, dict)
        cls.instance = None 

    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance = super(Singleton, cls).__call__(*args, **kw)
        return cls.instance


def getattr_r(target, current, target_list):
    import sys
    if current+1 == len(target_list):
        return getattr(target,target_list[current])
    return getattr_r(getattr(target,target_list[current]), current+1, target_list)

if __name__ == '__main__':
    import nose
    nose.run()
    raw_input()