
import types

from .. import *

def test_recursive_getattr():
    os = __import__('os')
    result = getattr_r(os, 0 , ['path','abspath'])
    assert(isinstance(result,types.FunctionType))
    assert(result.__name__ == 'abspath')
